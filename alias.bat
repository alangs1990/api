DOSKEY dcu=docker-compose up
DOSKEY dcub=docker-compose up --build
DOSKEY dck=docker-compose kill
DOSKEY dcd=docker-compose down

DOSKEY dnb=dotnet build src/
DOSKEY dnr=dotnet restore src/
DOSKEY dnp=dotnet publish src/ -c Release

DOSKEY efdu=dotnet ef database update -p src/api.csproj
DOSKEY efdd=dotnet ef database drop -p src/api.csproj
DOSKEY efma=dotnet ef migrations add $1 -p src/api.csproj
DOSKEY efmr=dotnet ef migrations remove -p src/api.csproj
DOSKEY efms=dotnet ef migrations script -p src/api.csproj -o ./publicar.sql
