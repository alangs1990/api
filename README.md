# README

## Instalação

Para inicializar o projeto sem problemas será necessário fazer a instalação de alguns programas, segue abaixo:

    Dotnet sdk 5.0
        https://dotnet.microsoft.com/download/dotnet/5.0
    
    AWS CLI
        https://awscli.amazonaws.com/AWSCLIV2.msi


### Migrations:

    O migration é utilizado para versionar o banco de dados, abaixo segue comandos.

##### Comandos:

    Atualizar o banco de dados (container deve estar rodando para poder usar este comando):
        dotnet ef database update

    Deletar o banco de dados (container deve estar rodando para poder usar este comando):
        dotnet ef database drop

    Criar uma nova versão do migration:
        dotnet ef migrations add {nome da versão}

    Remover último migration:
        dotnet ef migrations remove


### AWS CLI:

    Após instalado a AWS CLI, basta rodar o seguinte comando para configurar o :

    aws configure

    Será solicitado esses parametros e defina-os com as seguintes informações:

    AWS Access Key ID: access_key
    AWS Secret Access Key: secret_key
    Default region name: us-east-1
    Default output format:

##### Comandos:

    Criar Bucket S3:
        aws --endpoint-url=http://localhost:4572 s3 mb s3://servitek

    Copiar arquivo local para o Bucket S3:
        aws --endpoint-url=http://localhost:4572 s3 cp src/appsettings.json s3://servitek

    Listar arquivos do Bucket S3:
        aws --endpoint-url=http://localhost:4572 s3 ls s3://servitek

    Deletar arquivo do Bucket S3:
        aws --endpoint-url=http://localhost:4572 s3 rm s3://servitek/appsettings.json


### Docker

    Subir os serviços da Api, Banco de dados e LocalStack:
        docker-compose up --build

    Obs: Caso ocorrer algum erro para rodar o comando:
        apt-get update && apt-get install -y libgdiplus
    
    Basta rodar esse comando para reiniciar o WSL:
        wsl --shutdown


### Configurar Amazon Linux 2

    - Primeiro é necessário liberar o acesso SSH no grupo de segurança para qualquer IP referente a instancia do EC2.
    - Depois se conectar através do SSH pelo próprio console da AWS e rodar estes comandos:

    sudo yum install yum-utils
    sudo rpm --import "http://keyserver.ubuntu.com/pks/lookup?op=get&search=0x3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF"
    sudo yum-config-manager --add-repo http://download.mono-project.com/repo/centos/

    sudo yum clean all
    sudo yum makecache

    sudo yum install mono-complete

    - Ao concluir os comandos, precisa reiniciar a aplicação pelo Elastic Beanstalk

    OBS.: Isso é para poder ser gerado arquivos Excel
