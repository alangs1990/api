using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Api.Context;
using Microsoft.EntityFrameworkCore;

namespace Api.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly ApiContext _context;

        public Repository(ApiContext context)
        {
            _context = context;
        }

        public void Add(TEntity obj)
        {
            _context.Set<TEntity>().Add(obj);
        }

        public bool Exists(Func<TEntity, bool> predicate)
        {
            bool exists = false;

            var r = _context.Set<TEntity>().FirstOrDefault(predicate);

            if (r != null)
                exists = true;

            return exists;
        }

        public IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> predicate)
        {
            return GetAll().Where(predicate).AsQueryable();
        }

        public IQueryable<TEntity> GetAll()
        {
            return _context.Set<TEntity>();
        }

        public TEntity First()
        {
            return _context.Set<TEntity>().FirstOrDefault();
        }

        public async Task SaveAll()
        {
            await _context.SaveChangesAsync();
        }
    }
}