namespace Api
{
    public class CorsOptions
    {
        public string[] Origins { get; set; }
    }
}