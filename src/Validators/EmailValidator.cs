using System;
using System.Text.RegularExpressions;

namespace Api
{
    public class EmailValidator : IValidator<string>
    {
        public void Valid(string email)
        {
            if (email == null || !Regex.IsMatch(email, @"^[^@\s]+@[^@\s]+\.[^@\s]+$", RegexOptions.IgnoreCase))
                throw new Exception("Email não é válido");
        }
    }
}