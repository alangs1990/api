namespace Api
{
    public static class Validator
    {
        public static void Validar<T>(IValidator<T> validator, T objeto)
        {
            validator.Valid(objeto);
        }
    }
}