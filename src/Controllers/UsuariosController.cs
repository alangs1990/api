﻿using Api.Models;
using Api.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Api.api.Controllers
{
    [ApiController]
    [AllowAnonymous]
    [Route("[controller]")]
    public class UsuariosController : ControllerBase
    {
        private readonly IUsuariosServices _view;

        public UsuariosController(IUsuariosServices view)
        {
            _view = view;
        }

        [Produces("application/json")]
        [HttpPost()]
        public async Task<ActionResult> CadastrarUsuario([FromBody] Usuario instance)
        {
            await _view.Post(instance);
            return Ok();
        }

        [Produces("application/json")]
        [HttpPost("verificar-login")]
        public ActionResult<IEnumerable<string>> VerificarDisponibilidadeLogin([FromBody] Usuario instance) => Ok(_view.VerificarDisponibilidadeLogin(instance));
    }
}