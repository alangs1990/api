using Api.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Api.Services
{
    public interface IUsuariosServices
    {
        Task Post(Usuario instance);
        IEnumerable<string> VerificarDisponibilidadeLogin(Usuario instance);
    }
}