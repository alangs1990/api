using Api.Models;
using Api.Repository;
using Api.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Services
{
    public class UsuariosServices : IUsuariosServices
    {
        private readonly IRepository<Usuario> _repo;

        public UsuariosServices(IRepository<Usuario> repo)
        {
            _repo = repo;
        }

        public async Task Post(Usuario instance)
        {
            if (instance == null)
                throw new ArgumentException(message: "Objeto não pode ser nulo ou vazio.");

            if (_repo.Exists(u => u.Login == instance.Login))
                throw new ArgumentException(message: "Login já existente.");

            instance.Id = Guid.NewGuid();

            _repo.Add(instance);
            await _repo.SaveAll();
        }

        public IEnumerable<string> VerificarDisponibilidadeLogin(Usuario instance)
        {
            if (_repo.Exists(u => u.Login == instance.Login))
            {
                const int numMaxOpcoesLogin = 3;
                List<string> retorno = new List<string>();
                List<string> palavras = new List<string>();

                palavras.AddRange(instance.Nome.ToLower().Split(' '));
                palavras.Add(instance.Nascimento.Day.ToString());
                palavras.Add(instance.Nascimento.Month.ToString());
                palavras.Add(instance.Nascimento.Year.ToString());

                int index = instance.Email.IndexOf("@");
                string emailPrefix = instance.Email.Substring(0, index).ToLower();

                foreach (string palavra in palavras)
                {
                    index = emailPrefix.IndexOf(palavra);

                    if (index != -1)
                        emailPrefix = emailPrefix.Replace(palavra, "");
                }

                palavras.Add(emailPrefix);

                var random = new Random();
                string newLogin = "";
                bool percorrerLoop = true;
                int countTentativas = 0;
                int countTentativasNewLogin = 0;
                List<string> palavrasUtilizadas = new List<string>();
                while (percorrerLoop)
                {
                    newLogin = "";
                    palavrasUtilizadas.Clear();
                    countTentativasNewLogin = 0;

                    while (countTentativasNewLogin < 3)
                    {
                        string palavra = palavras[random.Next(palavras.Count)];

                        if (!palavrasUtilizadas.Any(a => a == palavra))
                        {
                            palavrasUtilizadas.Add(palavra);
                            newLogin += palavra;
                            countTentativasNewLogin++;
                        }
                    }

                    if (!retorno.Any(a => a == newLogin))
                    {
                        if (!_repo.Exists(u => u.Login == newLogin))
                        {
                            retorno.Add(newLogin);
                            countTentativas++;
                        }

                        if (countTentativas == numMaxOpcoesLogin)
                            percorrerLoop = false;
                    }
                }

                return retorno;
            }
            else
                return null;
        }
    }
}