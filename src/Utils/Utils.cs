using Newtonsoft.Json;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Api.Utils
{
    public static class Util
    {
        #region Criptografia
        private static readonly byte[] bIV =
        { 0x50, 0x08, 0xF1, 0xDD, 0xDE, 0x3C, 0xF2, 0x18,
        0x44, 0x74, 0x19, 0x2C, 0x53, 0x49, 0xAB, 0xBC };

        /// <summary>
        /// Representação de valor em base 64 (Chave Interna)
        /// O Valor representa a transformação para base64 de
        /// um conjunto de 32 caracteres (8 * 32 = 256bits)
        /// A chave é: "593547e6-35ed-11ec-8d3d-0242ac130003"
        /// </summary>
        private const string cryptoKey = "NTkzNTQ3ZTYtMzVlZC0xMWVjLThkM2QtMDI0MmFjMTMwMDAz";
        
        public static string Encrypt(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                byte[] bText = new UTF8Encoding().GetBytes(text);

                Rijndael rijndael = new RijndaelManaged();

                rijndael.KeySize = 256;

                MemoryStream mStream = new MemoryStream();
                
                CryptoStream encryptor = new CryptoStream(
                    mStream,
                    rijndael.CreateEncryptor(Convert.FromBase64String(cryptoKey), bIV),
                    CryptoStreamMode.Write);

                encryptor.Write(bText, 0, bText.Length);
                encryptor.FlushFinalBlock();

                return Convert.ToBase64String(mStream.ToArray());
            }
            else
            {
                return null;
            }
        }
        #endregion

        public static T Clonar<T>(T obj)
        {
            string r = JsonConvert.SerializeObject(obj, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
            return JsonConvert.DeserializeObject<T>(r);
        }

        #region Substitui caracteres especiais
        const string comAcentos = "ÄÅÁÂÀÃäáâàãÉÊËÈéêëèÍÎÏÌíîïìÖÓÔÒÕöóôòõÜÚÛüúûùÇç";
        const string semAcentos = "AAAAAAaaaaaEEEEeeeeIIIIiiiiOOOOOoooooUUUuuuuCc";

        public static string SubstituiCaracteresEspeciais(string input)
        {
            if (string.IsNullOrEmpty(input))
                return "";
            else
            {
                for (int i = 0; i < comAcentos.Length; i++)
                {
                    input = input.Replace(comAcentos[i].ToString(), semAcentos[i].ToString());
                }
                return input;
            }
        }
        #endregion
    }
}