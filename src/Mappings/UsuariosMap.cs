using Api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Api.Mappings
{
    public class UsuariosMap : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            builder.ToTable("Usuarios", "dbo");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).HasColumnName("Id").IsRequired();
            builder.Property(e => e.Nome).HasColumnName("Nome").HasMaxLength(255).IsRequired();
            builder.Property(e => e.Nascimento).HasColumnName("Nascimento").IsRequired();
            builder.Property(e => e.Email).HasColumnName("Email").HasMaxLength(255).IsRequired();
            builder.Property(e => e.Email).HasColumnName("Login").HasMaxLength(255).IsRequired();
            builder.Property(e => e.Senha).HasColumnName("Senha").HasMaxLength(100).IsRequired();
        }
    }
}