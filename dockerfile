# FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
FROM mcr.microsoft.com/dotnet/aspnet:5.0.2 AS base
WORKDIR /app
EXPOSE 80

# FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
FROM mcr.microsoft.com/dotnet/sdk:5.0.102-1 AS build
WORKDIR /src
COPY src/api.csproj api.csproj
RUN dotnet restore api.csproj
COPY src/. .
RUN dotnet build api.csproj -c Release -o /app/build
RUN dotnet publish api.csproj -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=build /app/publish .
ENTRYPOINT ["dotnet", "api.dll"]