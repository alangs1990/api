-- Insere usuariosPermissoes que estão faltando
Declare @idUsuario uniqueidentifier
DECLARE cursoruser CURSOR  
FOR select Id
	from Usuarios
OPEN cursoruser
FETCH NEXT FROM cursoruser

INTO @idUsuario
  
WHILE @@FETCH_STATUS = 0  
BEGIN
	insert into UsuariosPermissoes(Id, IdUsuario, IdPermissao, Permitido)
	select NEWID(), @idUsuario, p.Id, 0
	from Permissoes p
	where Id not in (select IdPermissao from UsuariosPermissoes where IdUsuario = @idUsuario)
	
	FETCH NEXT FROM cursoruser INTO @idUsuario
END

CLOSE cursoruser  
DEALLOCATE cursoruser
